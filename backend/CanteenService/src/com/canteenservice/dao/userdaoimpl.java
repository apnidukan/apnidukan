package com.canteenservice.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.canteenservice.pojo.userpojo;
import com.canteenservice.util.myConnection;

public class userdaoimpl implements userdao {

	
	@Override
	public boolean adduser(userpojo up) {
		try {
			
			Connection con=null;
			PreparedStatement ps=null;
			con=myConnection.connobj();
			ps=con.prepareStatement("insert into userdata (name,email,mobile,city,password) values (?,?,?,?,?)");
			ps.setString(1, up.getName());
			ps.setString(2,up.getEmail());
			ps.setInt(3, up.getMobile());
			ps.setString(4, up.getCity());
			ps.setString(5, up.getPassword());
			int i=ps.executeUpdate();
			
			if(i>0)
			return true;
			else
			return false;	
			
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
				
		}
		
		return false;
	}

	@Override
	public boolean deleteuser(String name) {
		
try {
			
			Connection con=null;
			PreparedStatement ps=null;
			con=myConnection.connobj();
			ps=con.prepareStatement("delete from userdata where name = ? ");
			ps.setString(1,name);
			int i=ps.executeUpdate();
			
			if(i>0)
			return true;
			else
			return false;	
		
		}
		catch(Exception e)
		{
			e.printStackTrace();
				
		}
		
		
		return false;
	}

	@Override
	public boolean updateuser(String name,int mobile) {
		try {
		Connection con=null;
		PreparedStatement ps=null;
		con=myConnection.connobj();
		ps=con.prepareStatement("update userdata set mobile = ? where name = ? ");
		ps.setInt(1, mobile);
		ps.setString(2,name);
		int i=ps.executeUpdate();
		
		if(i>0)
		return true;
		else
		return false;	
	
	}
	catch(Exception e)
	{
		e.printStackTrace();
			
	}
		return false;
	}

	@Override
	public List<userpojo> getAllUsers() {
		// TODO Auto-generated method stub
		

		try
        {
			Connection con=null;
			PreparedStatement ps=null;
			con=myConnection.connobj();
        	 ps = con.prepareStatement("select * from userdata");
        	
        	
        	ResultSet rs = ps.executeQuery();
        	List<userpojo> userList = new ArrayList<userpojo>();
        	while(rs.next())
        	{
        		userpojo userPojo = new userpojo();
        		userPojo.setId(rs.getInt("id"));
        		userPojo.setName(rs.getString("name"));
        		userPojo.setCity(rs.getString("city"));
        		
        		userPojo.setEmail(rs.getString("email"));
        		userPojo.setPassword(rs.getString("password"));
        		
        		userPojo.setMobile(rs.getInt("mobile"));
        		
        	
        		
        		userList.add(userPojo);
        		
        	}
        	return userList;
        	
        	
        }
        catch(Exception e)
        {
        	e.printStackTrace();
        }

		
	return null;
	}
	
}
