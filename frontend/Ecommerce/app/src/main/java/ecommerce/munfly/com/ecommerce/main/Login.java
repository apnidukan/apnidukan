package ecommerce.munfly.com.ecommerce.main;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Toast;
import android.app.AlertDialog;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;

import ecommerce.munfly.com.ecommerce.MainActivity;
import ecommerce.munfly.com.ecommerce.R;
import ecommerce.munfly.com.ecommerce.helper.InputValidation;
import ecommerce.munfly.com.ecommerce.modal.user;
import ecommerce.munfly.com.ecommerce.sql.DatabaseHelper;

public class Login extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG ="error" ;
    private final AppCompatActivity activity = Login.this;

    private ScrollView nestedScrollView;

    // private TextInputLayout textInputLayoutEmail;
    //private TextInputLayout textInputLayoutPassword;

    private EditText textInputEditTextEmail;
    private EditText textInputEditTextPassword;

    private AppCompatButton appCompatButtonLogin;

    private AppCompatTextView textViewLinkRegister;

    private InputValidation inputValidation;
    private DatabaseHelper databaseHelper;


    user user = new user();

    private JSONObject jsonObject;
    String response = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
          //getSupportActionBar().hide();


        initViews();
        initListeners();
        initObjects();
    }

    /**
     * This method is to initialize views
     */
    @SuppressLint("WrongViewCast")
    private void initViews() {

        nestedScrollView = (ScrollView) findViewById(R.id.nestedScrollView);

        //  textInputLayoutEmail = (TextInputLayout) findViewById(R.id.textInputLayoutEmail);
        //textInputLayoutPassword = (TextInputLayout) findViewById(R.id.textInputLayoutPassword);

        textInputEditTextEmail = (EditText) findViewById(R.id.text1);
        textInputEditTextPassword = (EditText) findViewById(R.id.textInputEditTextPassword);

        appCompatButtonLogin = (AppCompatButton) findViewById(R.id.ButtonLogin);

        textViewLinkRegister = (AppCompatTextView) findViewById(R.id.textViewLinkRegister);

    }

    /**
     * This method is to initialize listeners
     */
    private void initListeners() {
        appCompatButtonLogin.setOnClickListener(this);
        textViewLinkRegister.setOnClickListener(this);
    }

    /**
     * This method is to initialize objects to be used
     */
    private void initObjects() {
        databaseHelper = new DatabaseHelper(activity);
        inputValidation = new InputValidation(activity);

    }

    /**
     * This implemented method is to listen the click on view
     *
     * @param v
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ButtonLogin:
                BackgroundTask1 backgroundTask = new BackgroundTask1(Login.this);
                backgroundTask.execute();
                verifyFromSQLite();
                break;
            case R.id.textViewLinkRegister:
                // Navigate to RegisterActivity
                Intent intentRegister = new Intent(getApplicationContext(), Registration.class);
                startActivity(intentRegister);
                break;
        }
    }

    /**
     * This method is to validate the input text fields and verify login credentials from SQLite
     */
    public void verifyFromSQLite() {
        if (!inputValidation.isInputEditTextFilled(textInputEditTextEmail, getString(R.string.mail_error))) {
            return;
        }
        if (!inputValidation.isInputEditTextEmail(textInputEditTextEmail, getString(R.string.mail_error))) {
            return;
        }
        if (!inputValidation.isInputEditTextFilled(textInputEditTextPassword, getString(R.string.mail_error))) {
            return;
        }

        if (databaseHelper.checkUser(textInputEditTextEmail.getText().toString().trim()
                , textInputEditTextPassword.getText().toString().trim())) {


            Intent accountsIntent = new Intent(activity, ecommerce.munfly.com.ecommerce.MainActivity.class);
            accountsIntent.putExtra("EMAIL", textInputEditTextEmail.getText().toString().trim());
            emptyInputEditText();
            startActivity(accountsIntent);


        } else {
            Toast toast = Toast.makeText(getApplicationContext(), "email or psw is not found", Toast.LENGTH_SHORT);
            toast.show();
            // Snack Bar to show success message that record is wron
            //(nestedScrollView, getString(R.string.error_valid_email_password), Snackbar.LENGTH_LONG).show();
        }
    }

    /**
     * This method is to empty all input edit text
     */
    private void emptyInputEditText() {
        textInputEditTextEmail.setText(null);
        textInputEditTextPassword.setText(null);
    }


    @RequiresApi(api = Build.VERSION_CODES.CUPCAKE)
    @SuppressLint("NewApi")
    public class BackgroundTask1 extends AsyncTask<String, Void, String> {
        AlertDialog alertDialog;
        Context ctx;

        public BackgroundTask1(Context ctx) {
            this.ctx = ctx;
        }

        @Override
        protected void onPreExecute() {
            alertDialog = new AlertDialog.Builder(ctx).create();
            alertDialog.setTitle("Login Information....");
        }

        @Override
        protected String doInBackground(String... params) {

            /// http://localhost:8080/CanteenService/userservlet
            String reg_url = "http://192.168.0.104:8080/canteen/userservlet"; //Config.DOMAIN_URL+"AttendanceSystem/RegistrationServlet1";





            String jsonStr = makeServiceCall(reg_url);

            Log.e(TAG, "Response from url: " + jsonStr);

            if (jsonStr != null) try {
                // JSONObject jsonObj = new JSONObject(jsonStr);

                // Getting JSON Array node
                JSONArray contacts = new JSONArray(jsonStr);

                // looping through All Contacts
                for (int i = 0; i < contacts.length(); i++) {
                    JSONObject c = contacts.getJSONObject(i);

                    // String id = c.getString("id");
                    String mobile = c.getString("mobile");
                    String name = c.getString("name");
                    String city = c.getString("city");
                    String email = c.getString("email");
                    String password = c.getString("password");


                    // adding each child node to HashMap key => value
                    //user.setId(id);
                    user.setName(name);
                    user.setEmail(email);
                    user.setMobile(Integer.parseInt(mobile));
                    user.setPassword(password);
                    user.setCity(city);

                    // adding contact to contact list
                    databaseHelper.addUser(user);
                }
                return "Registration Success...";
            } catch (final JSONException e) {
                Log.e(TAG, "Json parsing error: " + e.getMessage());
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(),
                                "Json parsing error: " + e.getMessage(),
                                Toast.LENGTH_LONG)
                                .show();
                    }
                });

            }
            else {
                Log.e(TAG, "Couldn't get json from server.");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(),
                                "Couldn't get json from server. Check LogCat for possible errors!",
                                Toast.LENGTH_LONG)
                                .show();
                    }
                });
            }






            return "not";
        }


        @Override
        protected void onPostExecute (String result){
            if (result.equals("Registration Success...")) {
                Toast.makeText(ctx, "added all user", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(Login.this, MainActivity.class);
                startActivity(intent);
            } else {
                Toast.makeText(ctx, "not register successfully", Toast.LENGTH_LONG).show();
            }

        }
    }




    public String makeServiceCall(String reqUrl) {
        String response = null;
        try {
            URL url = new URL(reqUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setDoOutput(true);

            conn.setDoInput(true);
            OutputStream OS = conn.getOutputStream();
            BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(OS, "UTF-8"));
            String data = URLEncoder.encode("action", "UTF-8") + "=" + URLEncoder.encode("getAllUser", "UTF-8");
            bufferedWriter.write(data);
            bufferedWriter.flush();
            bufferedWriter.close();
            OS.close();
            conn.setRequestMethod("GET");
            // read the response
            InputStream in = new BufferedInputStream(conn.getInputStream());
            response = convertStreamToString(in);
        } catch (MalformedURLException e) {
            Log.e(TAG, "MalformedURLException: " + e.getMessage());
        } catch (ProtocolException e) {
            Log.e(TAG, "ProtocolException: " + e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, "IOException: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        }
        return response;
    }

    private String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line).append('\n');
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }
}
