package com.canteenservice.dao;

import java.util.List;

import com.canteenservice.pojo.userpojo;

public interface userdao {

	public boolean adduser(userpojo up);
	
	public boolean deleteuser(String name);
	
	public boolean updateuser(String name,int mobile);
	
	public List<userpojo> getAllUsers();
	
	
}
