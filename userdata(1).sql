-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 16, 2018 at 06:05 AM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `canteen`
--

-- --------------------------------------------------------

--
-- Table structure for table `userdata`
--

CREATE TABLE `userdata` (
  `id` int(11) DEFAULT NULL,
  `name` varchar(11) NOT NULL,
  `email` varchar(11) NOT NULL,
  `mobile` varchar(11) NOT NULL,
  `city` varchar(11) NOT NULL,
  `password` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `userdata`
--

INSERT INTO `userdata` (`id`, `name`, `email`, `mobile`, `city`, `password`) VALUES
(1, 'dhan', 'sd@gmail.co', '12345', 'tha', 'wad'),
(1, 'dhan', 'sd@gm', '12345', 'tha', 'wad'),
(NULL, 'dhan', 'dhwgmail', '123', 'than', 'wade'),
(NULL, 'gc', 'fcc', '123', 'fc', '123'),
(NULL, 'gc', 'fcc', '123', 'fc', '123'),
(NULL, 'dhan', 'n@gmail.co', '86532', 'thane', '12345'),
(NULL, 'name', 'q@gm.co', '3521', 'fcc', '12345'),
(NULL, 'cgg', 'q@g.con', '344', 'ddc', '12345'),
(NULL, 'Dhananjay', 'd@gmail.com', '76543', 'thane', '1234'),
(NULL, 'vivek', 'a@gmail.co', '567', 'rhb', '1345'),
(NULL, 'vivek', 'a@gm.co', '577', 'fgg', '123'),
(NULL, 'vivek', 'a@gm.co', '577', 'fgg', '123'),
(NULL, 'vivek', 'd@gm.co', '56', 'thh', '133'),
(NULL, 'fh', 'd@gmIl.co', '43', 'fg', '1234'),
(NULL, 'vivek', 'a@f.co', '45', 'thh', '223'),
(NULL, 'gh', 'a@g.co', '345', 'fgg', '133'),
(NULL, 'gh', 'a@g.co', '345', 'fgg', '133'),
(NULL, 'gh', 'a@g.co', '133', 'tgg', '123'),
(NULL, 'dgg', 'a@g.c', '234', 'rff', '123'),
(NULL, 'vivek', 'a@g.c', '555', 'fgg', '123'),
(NULL, 'df', 'a@g.c', '234', 'sxc', '123'),
(NULL, 'vivek', 'a@g.c', '34', 'ggg', '123'),
(NULL, 'vivek', 'a@g.c', '34', 'ggg', '123'),
(NULL, 'vivek', 'a@g.c', '45', 'tgg', '123'),
(NULL, 'vivek', 'a@g.c', '45', 'tgg', '123'),
(NULL, 'vivek', 'a@g.c', '45', 'tgg', '123'),
(NULL, 'vivek', 'a@g.c', '45', 'tgg', '123'),
(NULL, 'vivek', 'a@g.c', '45', 'tgg', '123'),
(NULL, 'vivek', 'a@g.c', '45', 'tgg', '123'),
(NULL, 'vivek', 'a@g.c', '45', 'tgg', '123'),
(NULL, 'vivek', 'a@g.c', '45', 'tgg', '123'),
(NULL, 'vivek', 'a@g.c', '45', 'tgg', '123'),
(NULL, 'vivek', 'a@g.c', '356', '123', '123'),
(NULL, 'vivek', 'a@g.co', '555', 'fgg', '123'),
(NULL, 'vivek', 'a@g.co', '555', 'fgg', '123'),
(NULL, 'vivek', 'a@g.c', '34', 'df', '123'),
(NULL, 'dgg', 'sdf', '23', 'thane', 'sdg'),
(NULL, 'dgg', 'sdf', '23', 'thane', 'sdg'),
(NULL, 'dgg', 'sdf', '23', 'thane', 'sdg'),
(NULL, 'dgg', 'sdf', '23', 'thane', 'sdg'),
(NULL, 'dgg', 'sdf', '23', 'thane', 'sdg'),
(NULL, 'dgg', 'sdf', '23', 'thane', 'sdg'),
(NULL, 'dgg', 'sdf', '23', 'thane', 'sdg'),
(NULL, 'dgg', 'sdf', '23', 'thane', 'sdg'),
(NULL, 'dgg', 'sdf', '23', 'thane', 'sdg'),
(NULL, 'dgg', 'sdf', '23', 'thane', 'sdg'),
(NULL, 'dgg', 'sdf', '23', 'thane', 'sdg'),
(NULL, 'dgg', 'sdf', '23', 'thane', 'sdg'),
(NULL, 'dgg', 'sdf', '23', 'thane', 'sdg'),
(NULL, 'dgg', 'sdf', '23', 'thane', 'sdg'),
(NULL, 'dgg', 'sdf', '23', 'thane', 'sdg'),
(NULL, 'dgg', 'sdf', '23', 'thane', 'sdg'),
(NULL, 'dgg', 'sdf', '23', 'thane', 'sdg'),
(NULL, 'dgg', 'sdf', '23', 'thane', 'sdg'),
(NULL, 'dgg', 'sdf', '23', 'thane', 'sdg'),
(NULL, 'dgg', 'sdf', '23', 'thane', 'sdg'),
(NULL, 'vivek', 'dghj', '23', 'thane', 'qwr'),
(NULL, 'vivek', 'qwe', '23', 'thane', 'qwe'),
(NULL, 'vivek', 'qwe', '23', 'thane', 'qwe'),
(NULL, 'vivek', 'gmail', '23', 'thane', 'qwe');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
