# Apni Dukan
E commerce platform for buyer to buy goods and services as per there requirements.

## Motivation
during the age of technology and usage of different platform by user which help to reduce completion of task time and increases the productivity of life and work. in that the mobile platform help to make portable and easy usable task execution tool. in traditional scenario buying goods and services required to lots of step to perform operation. we are trying to give better option with less effort for buy goods and services using mobile .   

## Build status
currently in developing stage. 

[![Build Status](https://travis-ci.org/akashnimare/foco.svg?branch=master)](https://travis-ci.org/akashnimare/foco)
[![Windows Build Status](https://ci.appveyor.com/api/projects/status/github/akashnimare/foco?branch=master&svg=true)](https://ci.appveyor.com/project/akashnimare/foco/branch/master)

## Code style

[![js-standard-style](https://img.shields.io/badge/code%20style-standard-brightgreen.svg?style=flat)](https://github.com/feross/standard)
 
## Screenshots
Include logo/demo screenshot etc.
<b>screenshots</b>
- [Prototype](https://bit.ly/2vZD0aA) (overview of project) 

## Tech/framework 
<b>language</b>
- [Java](https://www.java.com/) 

<b>Software</b>
- [Figma](https://www.figma.com/) (ui development tool) 
- [Android studio](https://developer.android.com/)( development tool for android )

## Contribute

Let people know how they can contribute into your project. A [contributing guideline](https://github.com/zulip/zulip-electron/blob/master/CONTRIBUTING.md) will be a big plus.

## Credits
@[rajkumar singh]()
@[dhananjay yadav]()
@[gaurav palashpagar]()

#### Anything else that seems useful

## License
 Apache License
                           Version 2.0, January 2004
                        http://www.apache.org/licenses/

   TERMS AND CONDITIONS FOR USE, REPRODUCTION, AND DISTRIBUTION


Apache © 