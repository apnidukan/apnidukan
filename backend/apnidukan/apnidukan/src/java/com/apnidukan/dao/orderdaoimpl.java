package com.apnidukan.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.apnidukan.pojo.orderpojo;
import com.apnidukan.util.myConnection;
import java.util.Date;

public class orderdaoimpl implements orderdao {



	
	public boolean updateorder(String name,String date) {
		try {
		Connection con=null;
		PreparedStatement ps=null;
		con=myConnection.connobj();
		ps=con.prepareStatement("update orderdata set date = ? where name = ? ");
		ps.setString(1,date);
		ps.setString(2,name);
		int i=ps.executeUpdate();
		
		if(i>0)
		return true;
		else
		return false;	
	
	}
	catch(Exception e)
	{
		e.printStackTrace();
			
	}
		return false;
	}

	public List<orderpojo> getAllorder() {
		// TODO Auto-generated method stub
		

		try
        {
			Connection con=null;
			PreparedStatement ps=null;
			con=myConnection.connobj();
        	 ps = con.prepareStatement("select * from orderdata");
        	
        	
        	ResultSet rs = ps.executeQuery();
        	List<orderpojo> orderList = new ArrayList<orderpojo>();
        	while(rs.next())
        	{
        		orderpojo orderPojo = new orderpojo();
        		orderPojo.setId(rs.getInt("id"));
        		orderPojo.setName(rs.getString("name"));
        		orderPojo.setCost(rs.getString("cost"));
        		
        		orderPojo.setDate(rs.getString("date"));
        		orderPojo.setPayment(rs.getString("payment"));
        		

        		
        	
        		
        		orderList.add(orderPojo);
        		
        	}
        	return orderList;
        	
        	
        }
        catch(Exception e)
        {
        	e.printStackTrace();
        }

		
	return null;
	}

    @Override
    public boolean adduser(orderdao up) {
        try {
			
			Connection con=null;
			PreparedStatement ps=null;
			con=myConnection.connobj();
			ps=con.prepareStatement("insert into orderdata (name,cost,date,payment) values (?,?,?,?)");
			ps.setString(1, up.getName());
			ps.setString(2,up.getCost());
			ps.setString(3, up.getDate());
			ps.setString(4, up.getPayment());
			int i=ps.executeUpdate();
			
			if(i>0)
			return true;
			else
			return false;	
			
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
				
		}
		
		return false;
    }

    @Override
    public boolean deleteuser(String name) {
        try {
			
			Connection con=null;
			PreparedStatement ps=null;
			con=myConnection.connobj();
			ps=con.prepareStatement("delete from orderdata where name = ? ");
			ps.setString(1,name);
			int i=ps.executeUpdate();
			
			if(i>0)
			return true;
			else
			return false;	
		
		}
		catch(Exception e)
		{
			e.printStackTrace();
				
		}
		
		
		return false;
    }

    @Override
    public boolean updateuser(String name, int mobile) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<orderdao> getAllUsers() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
	
}
