package com.apnidukan.dao;

import java.util.List;

import com.apnidukan.pojo.orderpojo;

public interface orderdao {

	public boolean adduser(orderpojo up);
	
	public boolean deleteuser(String name);
	
	public boolean updateuser(String name,String date);
	
	public List<orderdao> getAllorder();
     
	
	
}
