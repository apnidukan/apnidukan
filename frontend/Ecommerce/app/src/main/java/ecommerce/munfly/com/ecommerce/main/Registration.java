package ecommerce.munfly.com.ecommerce.main;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.app.AlertDialog;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

import ecommerce.munfly.com.ecommerce.R;
import ecommerce.munfly.com.ecommerce.sql.DatabaseHelper;
import ecommerce.munfly.com.ecommerce.modal.user;
public class Registration extends AppCompatActivity {
    EditText faculty_name;
    EditText faculty_email;
    EditText faculty_mobile;
    EditText faculty_password, city;
    Button btn_register;
    TextView btn_login;
    private user user;
    private DatabaseHelper databaseHelper;
    private final AppCompatActivity activity = Registration.this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registor);
       // getSupportActionBar().hide();
        faculty_name = (EditText) findViewById(R.id.name);
        faculty_email = (EditText) findViewById(R.id.email);
      //  faculty_mobile = (EditText) findViewById(R.id.mobile);
        faculty_password = (EditText) findViewById(R.id.password);
      btn_register = (Button) findViewById(R.id.btn_signup);
       btn_login=(TextView)findViewById(R.id.link_login);
     //   city = (EditText) findViewById(R.id.city);
        databaseHelper = new DatabaseHelper(activity);
        user = new user();

        btn_register.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.CUPCAKE)
            @Override
            public void onClick(View v) {
                String name = faculty_name.getText().toString();
                String email = faculty_email.getText().toString();
                //    String mobile = faculty_mobile.getText().toString();
          //      String cityname = city.getText().toString();
                String password = faculty_password.getText().toString();


                user.setName(name);
                user.setEmail(email);
//                user.setMobile(Integer.parseInt(mobile));
                user.setPassword(password);
             //   user.setCity(cityname);

                String method = "register";
                BackgroundTask1 backgroundTask = new BackgroundTask1(Registration.this);
                backgroundTask.execute(method, name, email, password);


            }
        });
        btn_login.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.CUPCAKE)
            @Override
            public void onClick(View v) {
                Intent intent =new Intent(Registration.this,Login.class);
                startActivity(intent);
            }
            });


    }

    private void postDataToSQLite() {

        //  if (!databaseHelper.checkUser(faculty_email.getText().toString().trim())) {



        databaseHelper.addUser(user);
        //  Toast toast = Toast.makeText(getApplicationContext(),R.string.success_message ,Toast.LENGTH_SHORT);
        // toast.show();
        // Snack Bar to show success message that record saved successfully


        // } else {
        //      // Snack Bar to show error message that record already exists
        //      Toast toast = Toast.makeText(getApplicationContext(),R.string.error_email_exists ,Toast.LENGTH_SHORT);
        //     toast.show();

        // }


    }

    @RequiresApi(api = Build.VERSION_CODES.CUPCAKE)
    @SuppressLint("NewApi")
    public class BackgroundTask1 extends AsyncTask<String, Void, String> {
        AlertDialog alertDialog;


        Context ctx;

        public BackgroundTask1(Context ctx) {
            this.ctx = ctx;
        }

        @Override
        protected void onPreExecute() {
            alertDialog = new AlertDialog.Builder(ctx).create();
            alertDialog.setTitle("Login Information....");
        }

        @Override
        protected String doInBackground(String... params) {

            /// http://localhost:8080/CanteenService/userservlet
            String reg_url = "http://192.168.0.104:8080/canteen/userservlet"; //Config.DOMAIN_URL+"AttendanceSystem/RegistrationServlet1";

            String method = params[0];
            if (method.equals("register")) {

                String name = params[1];
                String email = params[2];
                //String mobile = params[3];
                //String city = params[4];
                String password = params[3];
                try {
                    //String arrayToJson = objectMapper.writeValueAsString(videoMaster);
                    URL url = new URL(reg_url);
                    HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                    httpURLConnection.setRequestMethod("POST");
                    httpURLConnection.setDoOutput(true);

                    //httpURLConnection.setDoInput(true);
                    OutputStream OS = httpURLConnection.getOutputStream();
                    BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(OS, "UTF-8"));
                    String data = URLEncoder.encode("name", "UTF-8") + "=" + URLEncoder.encode(name, "UTF-8") + "&" +
                            URLEncoder.encode("email", "UTF-8") + "=" + URLEncoder.encode(email, "UTF-8") + "&" +
                           // URLEncoder.encode("mobile", "UTF-8") + "=" + URLEncoder.encode(mobile, "UTF-8") + "&" +
                           // URLEncoder.encode("city", "UTF-8") + "=" + URLEncoder.encode(city, "UTF-8") + "&" +
                            URLEncoder.encode("password", "UTF-8") + "=" + URLEncoder.encode(password, "UTF-8") + "&" +
                            URLEncoder.encode("action", "UTF-8") + "=" + URLEncoder.encode("Register", "UTF-8");
                    bufferedWriter.write(data);
                    bufferedWriter.flush();
                    bufferedWriter.close();
                    OS.close();
                    InputStream IS = httpURLConnection.getInputStream();
                    IS.close();
                    //httpURLConnection.connect();
                    httpURLConnection.disconnect();
                    postDataToSQLite();
                    return "Registration Success...";
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }


            return "not";
        }



        @Override
        protected void onPostExecute(String result) {
            if(result.equals("Registration Success..."))
            {
                Toast.makeText(ctx, result, Toast.LENGTH_LONG).show();
                Intent intent =new Intent(Registration.this,Login.class);
                startActivity(intent);
            }
            else
            {
                Toast.makeText(ctx, "not register successfully", Toast.LENGTH_LONG).show();
            }

        }
    }




}
