package com.canteenservice.servlet;

import java.io.IOException;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import org.json.simple.JSONObject;

import com.canteenservice.dao.userdao;
import com.canteenservice.dao.userdaoimpl;
import com.canteenservice.pojo.userpojo;


@WebServlet("/userservlet")
public class userservlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	public userservlet() {
        super();
       
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		
		//doPost(request, response);
		
		JSONObject json = new JSONObject();
		ObjectMapper objectMapper = new ObjectMapper();
		String action = request.getParameter("action");
		
		Enumeration en=request.getParameterNames();
		while(en.hasMoreElements())
		{
			Object objOri=en.nextElement();
			String param=(String)objOri;
			String value=request.getParameter(param);
			System.out.println("Parameter Name is '"+param+"' and Parameter Value is '"+value+"'");
		}	
			
		if (action!=null && action.equalsIgnoreCase("getAllUser")) 
		{
			
		userdao facultyDao= new userdaoimpl();
		List<userpojo> allfacultyList = facultyDao.getAllUsers();
		System.out.println(allfacultyList); 
				if(allfacultyList!=null)
							
				{
					objectMapper.enable(SerializationConfig.Feature.INDENT_OUTPUT);
					String arrayToJson = objectMapper.writeValueAsString(allfacultyList);
					response.getWriter().write(arrayToJson.toString());	
				}
				else
						{
							json.put("success","false");
							json.put("message","Please click restaurant");
							response.setContentType("application/json");
							response.setCharacterEncoding("UTF-8");
							response.getWriter().write(json.toString());
						}
		}
                else if (action!=null && action.equalsIgnoreCase("getorder")) 
		{
			
		userdao facultyDao= new userdaoimpl();
		List<userpojo> allfacultyList = facultyDao.getAllUsers();
		System.out.println(allfacultyList); 
				if(allfacultyList!=null)
							
				{
					objectMapper.enable(SerializationConfig.Feature.INDENT_OUTPUT);
					String arrayToJson = objectMapper.writeValueAsString(allfacultyList);
					response.getWriter().write(arrayToJson.toString());	
				}
				else
						{
							json.put("success","false");
							json.put("message","Please click restaurant");
							response.setContentType("application/json");
							response.setCharacterEncoding("UTF-8");
							response.getWriter().write(json.toString());
						}
		}
		else
		{
		}








		
		
		
		
		
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String a=request.getParameter("action");
		
		if (a!=null && a.equals("Register"))
		{
			
			String n=request.getParameter("name");
			String c="thane";
			String em=request.getParameter("email");
			int mo=Integer.parseInt("23");
			String pw=request.getParameter("password");
			
			userpojo up= new userpojo();
			up.setCity(c);
			up.setEmail(em);
			up.setMobile(mo);
			up.setPassword(pw);
			up.setName(n);
						
			userdao ud=new userdaoimpl();
			boolean b=ud.adduser(up);
			
			if(b)
				System.out.print("Done Add");
			else
				System.out.print(" Problem....");	
			
			
		}
		
		else if (a!=null && a.equals("Delete"))
		{
			
			
			String i=request.getParameter("name");
			
						
			userdao ud=new userdaoimpl();
			boolean b=ud.deleteuser(i);
			
			if(b)
				System.out.print("Done Delete");
			else
				System.out.print(" Problem....");	
			
			
		}
		else if (a!=null && a.equals("Update"))
		{
			
			
			String nm=request.getParameter("name");
			int mb=Integer.parseInt(request.getParameter("mobile"));
			
						
			userdao ud=new userdaoimpl();
			boolean b=ud.updateuser(nm,mb);
			
			if(b)
				System.out.print("Done Update");
			else
				System.out.print(" Problem....");	
			
			
		}
		
		doGet(request, response);
	}

}
